#include "pitches.h"

typedef struct QueueFunc {
  void (*ptr)();
  QueueFunc *next;
} QueueFunc;

void evalQueue(QueueFunc *func) {
  void (*ptr)();
  while (func) {
    ptr = func->ptr;
    ptr();
    func = func->next;
  }
}

const int RINGBUFFERSIZE = 30;
const int RINGWAITTIME = 100;
QueueFunc* ringBuffer[RINGBUFFERSIZE];
int ringPosition = 0;

void pushQueue(QueueFunc *func, int wait) {
  if (wait < 1 || wait > RINGBUFFERSIZE) {
    Serial.println("error1");
    return; // error
  }
  if (func->next) {
    Serial.println("error2");
    return; // already added
  }
  int pos = (wait + ringPosition) % RINGBUFFERSIZE;
  QueueFunc* target = ringBuffer[pos];
  if (!target) {
    ringBuffer[pos] = func;
  } else {
    QueueFunc* next = target->next;
    while (next) {
      target = next;
      next = next->next;
    }
    target->next = func;
  }
}

QueueFunc blinkFunc1;
const int BLINKLED1 = 13;
bool blinkState1 = false;

void blinkLoop1() {
  if (blinkState1) {
    blinkState1 = false;
    digitalWrite(BLINKLED1, HIGH);
  } else {
    blinkState1 = true;
    digitalWrite(BLINKLED1, LOW);
  }
  pushQueue(&blinkFunc1, 1);
}

int brightness1 = 0;
boolean reverseFade1 = false;
QueueFunc fadeFunc1;
const int FADEAMOUNT1 = 5;
const int FADEMIN1 = 0;
const int FADEMAX1 = 200;
const int FADELED1 = 9;

void fadeLoop1() {
  analogWrite(FADELED1, brightness1);
  if (reverseFade1) {
    brightness1 = brightness1 - FADEAMOUNT1;
  } else {
    brightness1 = brightness1 + FADEAMOUNT1;
  }
  if (brightness1 < FADEMIN1) {
    brightness1 = FADEMIN1;
  }
  if (brightness1 > FADEMAX1) {
    brightness1 = FADEMAX1;
  }
  if (brightness1 <= FADEMIN1 || brightness1 >= FADEMAX1) {
    reverseFade1 = !reverseFade1;
  }
  pushQueue(&fadeFunc1, 1);
}

int brightness2 = 0;
boolean reverseFade2 = false;
QueueFunc fadeFunc2;
const int FADEAMOUNT2 = 20;
const int FADEMIN2 = 20;
const int FADEMAX2 = 240;
const int FADELED2 = 10;

void fadeLoop2() {
  analogWrite(FADELED2, brightness2);
  if (reverseFade2) {
    brightness2 = brightness2 - FADEAMOUNT2;
  } else {
    brightness2 = brightness2 + FADEAMOUNT2;
  }
  if (brightness2 < FADEMIN2) {
    brightness2 = FADEMIN2;
  }
  if (brightness2 > FADEMAX2) {
    brightness2 = FADEMAX2;
  }
  if (brightness2 <= FADEMIN2 || brightness2 >= FADEMAX2) {
    reverseFade2 = !reverseFade2;
  }
  pushQueue(&fadeFunc2, 1);
}

int brightness3 = 0;
boolean reverseFade3 = false;
QueueFunc fadeFunc3;
const int FADEAMOUNT3 = 5;
const int FADEMIN3 = 30;
const int FADEMAX3 = 180;
const int FADELED3 = 5;

void fadeLoop3() {
  analogWrite(FADELED3, brightness3);
  if (reverseFade3) {
    brightness3 = brightness3 - FADEAMOUNT3;
  } else {
    brightness3 = brightness3 + FADEAMOUNT3;
  }
  if (brightness3 < FADEMIN3) {
    brightness3 = FADEMIN3;
  }
  if (brightness3 > FADEMAX3) {
    brightness3 = FADEMAX3;
  }
  if (brightness3 <= FADEMIN3 || brightness3 >= FADEMAX3) {
    reverseFade3 = !reverseFade3;
  }
  pushQueue(&fadeFunc3, 1);
}

int brightness4 = 0;
boolean reverseFade4 = false;
QueueFunc fadeFunc4;
const int FADEAMOUNT4 = 2;
const int FADEMIN4 = 10;
const int FADEMAX4 = 250;
const int FADELED4 = 6;

void fadeLoop4() {
  analogWrite(FADELED4, brightness4);
  if (reverseFade4) {
    brightness4 = brightness4 - FADEAMOUNT4;
  } else {
    brightness4 = brightness4 + FADEAMOUNT4;
  }
  if (brightness4 < FADEMIN4) {
    brightness4 = FADEMIN4;
  }
  if (brightness4 > FADEMAX4) {
    brightness4 = FADEMAX4;
  }
  if (brightness4 <= FADEMIN4 || brightness4 >= FADEMAX4) {
    reverseFade4 = !reverseFade4;
  }
  pushQueue(&fadeFunc4, 1);
}

const int MELODYLENGTH1 = 8;
int melody1[] = {
  //NOTE_C4, NOTE_G3, NOTE_G3, NOTE_A3, NOTE_G3, 0, NOTE_B3, NOTE_C4
  NOTE_C4, NOTE_D4, NOTE_E4, NOTE_F4, NOTE_A3, NOTE_D3, NOTE_B3, NOTE_C3
};
int noteDurations1[] = {
  5, 10, 10, 5, 5, 5, 5, 5,
};
int currentNote1 = -1;
QueueFunc melodyFunc1;
const int MELODYBUZZER1 = 3;
const int MELDOYLOOPWAIT1 = 20;
const int MELDOYPAUSE1 = 1;
bool melodyWaiting1;

void melodyLoop1() {
  if (melodyWaiting1) {
    melodyWaiting1 = false;
    currentNote1++;
    if (currentNote1 >= MELODYLENGTH1) {
      noTone(MELODYBUZZER1);
      currentNote1 = -1;
      pushQueue(&melodyFunc1, MELDOYLOOPWAIT1);
      return;
    }
    int nd = noteDurations1[currentNote1];
    if (nd <= MELDOYPAUSE1) {
      Serial.println("error 4");
    } else {
      int nd2 = nd - MELDOYPAUSE1;
      tone(MELODYBUZZER1, melody1[currentNote1], nd2 * RINGWAITTIME);
      pushQueue(&melodyFunc1, nd2);
    }
  } else {
    melodyWaiting1 = true;
    noTone(MELODYBUZZER1);
    pushQueue(&melodyFunc1, MELDOYPAUSE1);
  }
}

void setup() {
  Serial.begin(9600);
  pinMode(BLINKLED1, OUTPUT);
  pinMode(FADELED1, OUTPUT);
  pinMode(FADELED2, OUTPUT);
  pinMode(FADELED3, OUTPUT);
  pinMode(FADELED4, OUTPUT);
  blinkFunc1.ptr = &blinkLoop1;
  fadeFunc1.ptr = &fadeLoop1;
  fadeFunc2.ptr = &fadeLoop2;
  fadeFunc3.ptr = &fadeLoop3;
  fadeFunc4.ptr = &fadeLoop4;
  melodyFunc1.ptr = &melodyLoop1;
  blinkLoop1();
  fadeLoop1();
  fadeLoop2();
  fadeLoop3();
  fadeLoop4();
  melodyLoop1();
}

void loop() {
  QueueFunc* target = ringBuffer[ringPosition];
  if (target) {
    ringBuffer[ringPosition] = NULL;
    QueueFunc* next;
    delay(20);
    do {
      next = target->next;
      target->next = NULL;
      target->ptr();
      target = next;
    } while (target);
  }
  ringPosition++;
  if (ringPosition == RINGBUFFERSIZE) {
    ringPosition = 0;
  }
  delay(RINGWAITTIME);
}
